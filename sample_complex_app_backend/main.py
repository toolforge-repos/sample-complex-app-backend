import datetime
import http
import logging

import httpx
from celery.result import AsyncResult
from fastapi import FastAPI, HTTPException, Request
from fastapi.responses import JSONResponse
from sqlmodel import select

from .config import Settings
from .db import engine, get_latest_alembic_revision, get_session
from .models import DBCheck
from .schemas import (
    DBCheckResponse,
    HelloResponse,
    ScheduledJobCheckResponse,
    TaskResponse,
    TaskStatus,
)
from .tasks import heartbeat_task

app = FastAPI(docs_url=None, redoc_url=None, title="Sample complex app API")


@app.get("/")
def root() -> HelloResponse:
    return HelloResponse(message="Hello world!")


@app.get("/task")
def task_check() -> TaskStatus:
    """Not doing it async because we don't need to, and makes the frontend side considerably easier."""
    async_result: AsyncResult[TaskResponse] = heartbeat_task.delay()
    result = async_result.get(timeout=60, interval=0.2)
    return TaskStatus(
        task_id=async_result.task_id,
        status=async_result.state,
        result=result,
    )


@app.get("/database")
def database_check() -> DBCheckResponse:
    statuses: list[str] = []
    try:
        new_dbcheck_entry = DBCheck(
            timestamp=datetime.datetime.now(tz=datetime.timezone.utc)
        )
        with get_session() as session:
            session.add(new_dbcheck_entry)
            session.commit()
    except Exception as error:
        statuses.append(f"write_error({error})")

    db_check = DBCheck(id=None, timestamp="")
    try:
        with get_session() as session:
            db_check = session.exec(select(DBCheck).limit(1)).first() or db_check
    except Exception as error:
        statuses.append(f"read_error({error})")

    revision = "unable to get revision"
    try:
        revision = get_latest_alembic_revision() or revision
    except Exception as error:
        statuses.append(f"revision_error({error})")

    db_host = str(engine.url)
    if "@" in db_host:
        db_host = db_host.rsplit("@", 1)[-1]

    if statuses:
        raise HTTPException(
            status_code=http.HTTPStatus.INTERNAL_SERVER_ERROR,
            detail=DBCheckResponse(
                db_check=db_check,
                db_host=db_host,
                db_upgrade_revision=revision,
                status=f"ERROR({','.join(statuses)})",
            ).model_dump(),
        )

    return DBCheckResponse(
        db_check=db_check,
        db_host=db_host,
        db_upgrade_revision=revision,
        status="OK",
    )


@app.get("/scheduled_job")
async def scheduled_job_check() -> ScheduledJobCheckResponse:
    settings = Settings()
    try:
        response = await httpx.AsyncClient(
            headers={"user_agent": "sample-complex-app-backend"}
        ).get(url=settings.scheduled_job_url)
        data = response.json()
        response.raise_for_status()
        gotten_response = ScheduledJobCheckResponse.model_validate(data)

    except Exception as error:
        logging.exception("ERROR: %s", error)
        return ScheduledJobCheckResponse(
            status=f"ERROR ({error})",
            last_run=str(datetime.datetime.now(tz=datetime.timezone.utc)),
        )

    last_run_time = datetime.datetime.strptime(
        gotten_response.last_run, "%a %b %d %H:%M:%S %Z %Y"
    ).replace(tzinfo=datetime.timezone.utc)
    current_time = datetime.datetime.now(tz=datetime.timezone.utc)

    if current_time - last_run_time > datetime.timedelta(hours=2):
        gotten_response.status = "ERROR (more than 2h passed since the last check)"

    return gotten_response


@app.exception_handler(HTTPException)
async def httpexception_handler(request: Request, exc: HTTPException) -> JSONResponse:
    """Unwrap the `detail` key that fastapi adds by default to errors and just return the object in it.

    > raise HTTPException(detail={"my": "object"})
    From:
       {
         "detail": {"my": "object"}
       }

    To:
       {
         "my": "object"
       }
    """
    return JSONResponse(status_code=exc.status_code, content=exc.detail)
