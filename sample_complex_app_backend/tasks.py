from celery import Celery
from datetime import datetime

from .config import Settings
from .schemas import TaskResponse, register_pydantic_types

celery_app = Celery(__name__)
celery_app.config_from_object(Settings())
# Reconnection settings
celery_app.conf.broker_transport_options = {"retry_policy": {"timeout": 5.0}}
celery_app.conf.result_backend_transport_options = {"retry_policy": {"timeout": 5.0}}

register_pydantic_types(TaskResponse)


@celery_app.task
def heartbeat_task() -> TaskResponse:
    now = datetime.now().strftime("%Y-%m-%dT%H:%M:%s")
    return TaskResponse(
        message="I'm alive!",
        timestamp=now,
    )
