from sqlmodel import Field, SQLModel


class DBCheck(SQLModel, table=True):
    id: int = Field(default=None, nullable=False, primary_key=True)
    timestamp: str
