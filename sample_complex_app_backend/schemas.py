from pydantic import BaseModel
from typing import Any, Type
from functools import partial

# see https://github.com/sbdchd/celery-types/issues/155
from kombu.utils import json  # type: ignore

from .models import DBCheck


# pydantic support available only in future celery 5
# see https://github.com/celery/celery/issues/8751
def class_full_name(clz: Type[BaseModel]) -> str:
    return ".".join([clz.__module__, clz.__qualname__])


def _encoder(obj: BaseModel, *args: Any, **kwargs: Any) -> dict[str, Any]:
    return obj.model_dump(*args, **kwargs)


def _decoder(clz: Type[BaseModel], data: dict[str, Any]) -> BaseModel:
    return clz(**data)


def register_pydantic_types(*models: Type[BaseModel]) -> None:
    for model in models:
        json.register_type(
            model,
            class_full_name(model),
            encoder=_encoder,
            decoder=partial(_decoder, model),
        )


class HelloResponse(BaseModel):
    message: str


class TaskResponse(BaseModel):
    message: str
    timestamp: str


class TaskStatus(BaseModel):
    task_id: str
    status: str
    result: TaskResponse | None = None


class DBCheckResponse(BaseModel):
    db_check: DBCheck
    db_host: str
    db_upgrade_revision: str
    status: str


class ScheduledJobCheckResponse(BaseModel):
    status: str
    last_run: str
