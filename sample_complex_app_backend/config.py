from pydantic_settings import BaseSettings


class Settings(BaseSettings):
    result_backend: str = "redis://127.0.0.1:6379/0"
    broker_url: str = "redis://127.0.0.1:6379/0"
    task_default_queue: str = "sample-complex-app-tasks"
    database_url: str = "sqlite:///./dev.db"
    scheduled_job_url: str = (
        "https://tools-static.wmflabs.org/sample-complex-app/cronjob_status.json"
    )
