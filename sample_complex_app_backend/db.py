from sqlmodel import create_engine, SQLModel, Session, text
from typing import Generator
from contextlib import contextmanager

from .config import Settings


engine = create_engine(Settings().database_url)


def init_db() -> None:
    SQLModel.metadata.create_all(engine)


def get_latest_alembic_revision() -> str | None:
    maybe_revision = (
        engine.connect()
        .execute(statement=text("SELECT version_num from alembic_version"))
        .first()
    )

    if maybe_revision:
        return str(maybe_revision[0])

    return None


@contextmanager
def get_session() -> Generator[Session, None, None]:
    with Session(engine) as session:
        yield session
