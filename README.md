# Toolforge sample complex application - backend

This is a sample application to measure and demonstrate different features of
the toolforge platform.

It's meant to be rather complex so it uses many features at the same time, for
simpler and more-specific examples you can
[https://toolhub.wikimedia.org/search?q=sample-&ordering=-score&page=1&page_size=12](search
for 'sample-\*' tools on toolhub).

# Adding dependencies

Remember to generate the `requirements.txt` file after adding/removing
dependencies:

```
> poetry export --without-hashes --format=requirements.txt > requirements.txt
```

# Adding a new database migration

Use the usual alembic command:

```
> poetry run alembic revision --autogenerate -m 'I added this and that'
```

# Running all the migrations

They are run by default when you bring up the docker-compose.yaml services, but
to do by hand you can:

```
> poetry run alembic upgrade head
```

# Running locally

There's a `docker-compose.yaml` file that should get you running.

## Start everything

```
> podman-compose up
```

# Deploying in toolforge

## Building in toolforge

Ssh to the bastion and run:

```
mylaptop> ssh login.toolforge.org
myuser@toolforge> become sample-complex-app
sample-complex-app@toolforge> toolforge build start --image-name backend https://gitlab.wikimedia.org/toolforge-repos/sample-complex-app-backend
```

## Running in toolforge

You can just load the jobs.yaml file:

```
sample-complex-app@toolforge> toolforge jobs load jobs.yaml
```
