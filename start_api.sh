#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail


uvicorn \
    --host "${API_HOST:-0.0.0.0}" \
    --port "${API_PORT:-8080}" \
    --log-level "${API_LOGLEVEL:-debug}" \
    sample_complex_app_backend.main:app
