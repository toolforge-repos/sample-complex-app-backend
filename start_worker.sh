#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail


celery \
    --app sample_complex_app_backend.tasks.celery_app \
    worker \
    --loglevel INFO
