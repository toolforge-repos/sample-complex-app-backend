#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail


alembic upgrade head
