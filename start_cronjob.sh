#!/bin/bash

set -o nounset
set -o errexit
set -o pipefail

echo '{"status": "OK", "last_run": "'"$(date)"'"}' | tee "$TOOL_DATA_DIR"/www/static/cronjob_status.json
